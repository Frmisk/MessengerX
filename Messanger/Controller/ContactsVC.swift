//
//  ContactsVC.swift
//  Messanger
//
//  Created by Farid on 9/10/18.
//  Copyright © 2018 Farid. All rights reserved.
//

import UIKit
import ContactsUI

class ContactsVC: UIViewController {
    
    var searchController: UISearchController!
    
    // Table view configuration
    private let cellId = "contactCell"
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.separatorColor = .clear
        tv.allowsMultipleSelection = false
        tv.delegate = self
        tv.dataSource = self
        tv.register(ContactCell.self, forCellReuseIdentifier: self.cellId)
        
        return tv
    }()
    
    func setupViews() {
        view.backgroundColor = UIColor.SpringWood
        
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMenuButton()
        
        setupViews()
        configureSearchController()
        fetchContacts()
    }
    
    func setupMenuButton() {
        //create a new button
        let button: UIButton = UIButton(type: .custom)
        //set image for button
        button.setImage(UIImage(named: "menu"), for: .normal)
        //add function for button
        button.addTarget(self, action: #selector(menuAction), for: .touchUpInside)
        //set frame
        button.frame = CGRect(x: 0, y: 0, width: 39, height: 38)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func menuAction() {
        print("tapped")
    }
    
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var filteredContacts = [CNContact]()
    var shouldShowSearchResults = false
    
    func fetchContacts() {
        let keys = [
            CNContactThumbnailImageDataKey,
            CNContactImageDataKey,
            CNContactImageDataAvailableKey,
            CNContactPhoneNumbersKey,
            CNContactPostalAddressesKey,
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName)
        ] as [Any]
        
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        do {
            try contactStore.enumerateContacts(with: request) {
                (contact, stop) in

                self.contacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
    }

}

extension ContactsVC: UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search here..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
        //tableView.tableHeaderView = searchController.searchBar
    }

    
    func updateSearchResults(for searchController: UISearchController) {
        
        self.filteredContacts.removeAll(keepingCapacity: false)
        guard let searchText = searchController.searchBar.text else {
            return
        }
        
        let array = contacts.filter {
            return $0.givenName.range(of: searchText) != nil ||
                $0.familyName .range(of: searchText) != nil
                //$0.ManagerDesignation.range(of: searchText) != nil
        }
        
        self.filteredContacts = array
        
        self.tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tableView.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tableView.reloadData()
        }
        
        searchController.searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredContacts.count
        }
        else {
            return contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! ContactCell
        
        if self.shouldShowSearchResults {
            cell.configure(contact: filteredContacts[indexPath.row])
        } else {
            cell.configure(contact: contacts[indexPath.row])
        }
        
//        cell.voiceCallButton.addTarget(self, action: #selector(startVoiceCall), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = ContactsHeaderUIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    
//    @objc func startVoiceCall() {
//        let vc = VoiceCallVC()
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let voiceCall = voiceCallAction(at: indexPath)
        let videoCall = videoCallAction(at: indexPath)

        return UISwipeActionsConfiguration(actions: [voiceCall, videoCall])
    }

    func voiceCallAction(at indexPath: IndexPath) -> UIContextualAction {
        let contact = contacts[indexPath.row]

        let action = UIContextualAction(style: .normal, title: "Voice Call") { (action, view, completion) in
            completion(true)

            let vc = VoiceCallVC()
            
            vc.contact = contact
            self.navigationController?.pushViewController(vc, animated: true)
        }

        action.image = UIImage(named: "camera")
        action.backgroundColor = UIColor.Apricot

        return action
    }

    func videoCallAction(at indexPath: IndexPath) -> UIContextualAction {
        //let contact = contacts[indexPath.row]

        let action = UIContextualAction(style: .normal, title: "Video Call") { (action, view, completion) in
            completion(true)
        }

        action.image = UIImage(named: "camera")
        action.backgroundColor = UIColor.Apricot

        return action
    }
}

class ContactsHeaderUIView: UIView {
    
    let searchButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "search"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        return button
    }()
    
    let title: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Contacts"
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(searchButton)
        addSubview(title)
        
        searchButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        searchButton.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        title.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        title.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ContactCell: UITableViewCell {
    
    let name: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "IRANSansMobile-Light", size: 16)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let phone: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "IRANSansMobile-Light", size: 16)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let avatar: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 27
        img.clipsToBounds = true
        img.frame = CGRect(x: 16, y: 0, width: 54, height: 54)
        
        return img
    }()
    
    let voiceCallButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "voice"), for: .normal)
        
        return button
    }()
    
    let videoCallButton: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = UIImage(named: "camera")
        
        return img
    }()
    
    func configure(contact: CNContact) {
        let name = contact.givenName + contact.familyName
        
        self.name.text = name
        
        if contact.phoneNumbers.count > 0 {
            self.phone.text = (contact.phoneNumbers[0].value).value(forKey: "digits") as? String
        }
        
        if let imageData = contact.imageData {
            avatar.image = UIImage(data: imageData)
        } else {
            avatar.setImage(string:name, color: UIColor.colorHash(name: name), circular: true)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.SpringWood
        
        contentView.addSubview(name)
        contentView.addSubview(phone)
        contentView.addSubview(avatar)
        
//        contentView.addSubview(voiceCallButton)
//        contentView.addSubview(videoCallIcon)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        name.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16 + 44 + 16).isActive = true
        name.heightAnchor.constraint(equalToConstant: 24).isActive = true
        name.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -16).isActive = true
        
        phone.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16 + 44 + 16).isActive = true
        phone.heightAnchor.constraint(equalToConstant: 16).isActive = true
        phone.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 8).isActive = true
        
        avatar.frame = CGRect(x: 16, y: (contentView.frame.height / 2) - (54 / 2), width: 54, height: 54)
        
//        voiceCallButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16).isActive = true
//        voiceCallButton.widthAnchor.constraint(equalToConstant: 42).isActive = true
//        voiceCallButton.heightAnchor.constraint(equalToConstant: 42).isActive = true
//        voiceCallButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
//
//        videoCallIcon.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -42 - 16 - 16).isActive = true
//        videoCallIcon.widthAnchor.constraint(equalToConstant: 42).isActive = true
//        videoCallIcon.heightAnchor.constraint(equalToConstant: 42).isActive = true
//        videoCallIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
    }
    
}
