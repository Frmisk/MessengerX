//
//  APIManager.swift
//  Messanger
//
//  Created by Farid on 9/10/18.
//  Copyright © 2018 Farid. All rights reserved.
//

import Foundation
import Alamofire

struct APIError: Decodable {
    let haserror: Bool
    let code: Int
}

class APIManager {
    
    private init() {}
    
    static let shared = APIManager()
    
    func execute(_ url: URL, params: [String : Any]?, headers: [String: String]? = [:], success:@escaping (Data) -> Void, failure:@escaping (Data, Int) -> Void) {
        
        var secturyParams = params
        secturyParams!["mobile"] = Config.mobile
        secturyParams!["token"] = Config.token
        
        Alamofire.request(url, method: .post , parameters: secturyParams! , encoding: URLEncoding.httpBody , headers: headers).responseJSON{ response in
            
            switch(response.result) {
            case .success(_):
                
                if let data = response.data {
                    
                    do {
                        let error = try JSONDecoder().decode(APIError.self, from: data)
                        
                        if error.haserror == false {
                            success(data)
                        } else {
                            failure(data, error.code)
                        }
                        
                    }catch{}
                    
                }
                
                break
            case .failure(_):
                let error: Error = response.result.error!
                print("❤️ Fetal Error -> \(error)")
            }
            
        }
        // End request
    }
}
